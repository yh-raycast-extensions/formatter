import { Clipboard, Detail, PopToRootType, showHUD } from "@raycast/api";
import { useEffect, useState } from "react";

const TITLE = "JSON Formatter";

type State = { state: "INIT" } | { state: "ERROR"; error: any };

export default function Command() {
  const [state, setState] = useState<State>({ state: "INIT" });

  useEffect(() => {
    let isMounted = true;

    (async () => {
      try {
        const text = await Clipboard.readText();
        if (!isMounted) {
          return;
        }

        if (!text) {
          await showHUD("Clipboard is empty", { popToRootType: PopToRootType.Immediate });
          return;
        }

        const content = JSON.parse(text);
        const formatted = JSON.stringify(content, undefined, 2);
        await Clipboard.copy(formatted);
        await showHUD("Formatted JSON", { popToRootType: PopToRootType.Immediate });
      } catch (error) {
        if (!isMounted) {
          return;
        }
        setState({ state: "ERROR", error });
      }
    })();

    return () => {
      isMounted = false;
    };
  }, []);

  if (state.state === "INIT") {
    return <Detail navigationTitle={TITLE} isLoading={true} markdown="Formatting your JSON" />;
  }

  const markdown = `
  # Failed to format JSON

  \`\`\`
  ${state.error.toString()}
  \`\`\`
  `;

  return <Detail navigationTitle={TITLE} markdown={markdown} />;
}
